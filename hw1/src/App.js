import React from "react";
import "./App.css";
import styled from "styled-components";
import Menu from "./hw1/Menu";
import Carousel from "./hw1/Carousel";
import Card from "./hw1/Card";
import Form from "./hw1/Form";
import Footer from "./hw1/Footer";

const Layout = styled.div`
  /* moved styles to ./style/style.scss */
`;
const Title = styled.h1`
  grid-area: ${(props) => props.gridArea};
  /* moved styles to ./style/style.scss */
`;
const Heading = styled.h2`
  grid-area: h;
  /* moved styles to ./style/style.scss */
`;

export default (props) => {
  return (
    <Layout className="home-container-responsive">
      <Heading>HW1 9526013 Armin Ayatollahi </Heading>
      <Title gridArea="h1" id="cards">
        Cards
      </Title>
      <Card gridArea="c1" />
      <Card gridArea="c2" />
      <Card gridArea="c3" />
      <Title gridArea="h2" id="menu">
        Menu
      </Title>
      <Menu />
      <Title gridArea="h3" id="carousel">
        Carousel
      </Title>
      <Carousel />
      <Title gridArea="h4" id="form">
        Form
      </Title>
      <Form />
      <Footer />
    </Layout>
  );
};
