import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import styled from "styled-components";

const useStyles = makeStyles({
  root: {
    maxWidth: 345,
    fontFamily: "EbGaramond",
  },
  media: {
    height: 140,
  },
});

const Container = styled.div`
  grid-area: ${(props) => props.gridArea};
  display: flex;
  justify-content: center;
  align-items: center;
`;

export default (props) => {
  const classes = useStyles();

  return (
    <Container gridArea={props.gridArea} className="animated bounceIn slow">
      <Card className={classes.root}>
        <CardActionArea>
          <CardMedia
            className={classes.media}
            image="/logo512.png"
            title="Contemplative Reptile"
          />
          <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
              React
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
              React is a JavaScript library for building user interfaces. It is
              maintained by Facebook and a community of individual developers
              and companies.
            </Typography>
          </CardContent>
        </CardActionArea>
        <CardActions>
          <Button size="small" color="primary">
            Share
          </Button>
          <Button size="small" color="primary">
            Learn More
          </Button>
        </CardActions>
      </Card>
    </Container>
  );
};
