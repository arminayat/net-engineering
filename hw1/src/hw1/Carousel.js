import React, { PureComponent } from "react";
import styled from "styled-components";
import Carousel from "react-multi-carousel";
import { MdChevronLeft, MdChevronRight } from "react-icons/md";
import "react-multi-carousel/lib/styles.css";

const Container = styled.div`
  border-radius: 2rem;
  grid-area: s;
`;

const Image = styled.img`
  align-items: center;
  /* border-radius: 20px; */
  width: 100%;
  height: 100%;
`;

const SliderPrevButton = styled.a`
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 0 2rem;
  position: absolute;
  cursor: pointer;
  background-color: #614385;
  width: 4rem;
  height: 4rem;
  border-radius: 100px;
  cursor: pointer;
`;

const SliderNextButton = styled(SliderPrevButton)`
  right: 0px;
  background-color: #516395;
`;

export default (props) => {
  const responsive = {
    superLargeDesktop: {
      // the naming can be any, depends on you.
      breakpoint: { max: 4000, min: 3000 },
      items: 2,
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 1,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 2,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
    },
  };
  return (
    <Container>
      <Carousel
        responsive={responsive}
        ssr
        showDots
        infinite
        autoPlay
        autoPlaySpeed={5000}
        transitionDuration={500}
        customRightArrow={
          <SliderNextButton transparentButton={props.transparentButton}>
            <MdChevronRight style={{ fontSize: "2.5rem", color: "white" }} />
          </SliderNextButton>
        }
        customLeftArrow={
          <SliderPrevButton transparentButton={props.transparentButton}>
            <MdChevronLeft style={{ fontSize: "2.5rem", color: "white" }} />
          </SliderPrevButton>
        }
      >
        <Image src="http://placehold.it/1000x400/ffffff/c0392b/&text=slide1" />
        <Image src="http://placehold.it/1000x400/ffffff/c0392b/&text=slide2" />
        <Image src="http://placehold.it/1000x400/ffffff/c0392b/&text=slide3" />
        <Image src="http://placehold.it/1000x400/ffffff/c0392b/&text=slide4" />
        <Image src="http://placehold.it/1000x400/ffffff/c0392b/&text=slide5" />
        <Image src="http://placehold.it/1000x400/ffffff/c0392b/&text=slide6" />
      </Carousel>
    </Container>
  );
};
