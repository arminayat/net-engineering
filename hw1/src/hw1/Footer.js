import React, { PureComponent } from "react";
import styled from "styled-components";

const Footer = styled.footer`
  display: flex;
  grid-area: b;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  height: 5rem;
  width: 100%;
  background-color: white;
`;
const Link = styled.a`
  font-size: 1.4rem;
  margin: 0 3rem;
`;
export default (props) => {
  return (
    <Footer>
      <Link href="#cards">Cards</Link>
      <Link href="#menu">Menu</Link>
      <Link href="#carousel">Carousel</Link>
      <Link href="#form">Form</Link>
    </Footer>
  );
};
