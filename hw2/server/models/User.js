const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const UserSchema = new Schema({
  username: {
    type: String,
    required: true,
  },
  age: {
    type: Number,
    required: true,
  },
  phone: {
    type: String,
    default: true,
  },
  email: {
    type: String,
    required: true,
  },
});
module.exports = mongoose.model("users", UserSchema);
