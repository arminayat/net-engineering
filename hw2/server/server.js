const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const cors = require("cors")
const app = express();
//enabling all cors requests
app.use(cors());
// Load User model
const User = require("./models/User");
// Bodyparser middleware
app.use(
  bodyParser.urlencoded({
    extended: false,
  })
);
app.use(bodyParser.json());
// DB Config
const db = require("./keys").mongoURI;
// Connect to MongoDB
mongoose
  .connect(db, { useNewUrlParser: true })
  .then(() => console.log("MongoDB successfully connected"))
  .catch((err) => console.log(err));

app.get("/", function (req, res) {
  User.find({})
    .then((users) => res.json(users))
    .catch(() => res.status(400));
});

app.post("/register", (req, res) => {
  User.findOne({ username: req.body.username }).then((user) => {
    if (user) {
      return res.status(400).json({ error: "username already exists" });
    } else {
      const newUser = new User({
        username: req.body.username,
        age: req.body.age,
        phone: req.body.phone,
        email: req.body.email,
      });
      newUser
        .save()
        .then(() => res.send("success").status(200))
        .catch((error) => res.send(error).status(400));
    }
  });
});

app.listen(3001, function () {
  console.log("listening on 3001");
});
