import React, { useState, useEffect } from "react";
import logo from "./logo.svg";
import "./App.css";
import Form from "./Form";
import styled from "styled-components";
import axios from "axios";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { makeStyles, Backdrop, CircularProgress } from "@material-ui/core";

const Layout = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;
  background: linear-gradient(to right, #614385, #516395);
  padding-top: 7rem;
`;
const RefetchButton = styled.button`
  display: flex;
  justify-content: center;
  align-items: center;
  border: 2px solid white;
  background-color: transparent;
  font-size: 20px;
  color: white;
  cursor: pointer;
  border-radius: 10px;
  margin-bottom: 50px;
  height: 50px;
  width: 250px;
  :hover {
    background-color: rgba(255, 255, 255, 0.1);
  }
`;

const useStyles = makeStyles({
  table: {
    minWidth: 500,
  },
  tableContainer: {
    width: "30%",
    marginBottom: "50px",
  },
  backdrop: {
    zIndex: 10,
    color: "#fff",
  },
});

function App() {
  const classes = useStyles();
  const [users, setUsers] = useState([]);
  const [loading, setLoading] = useState(false);
  const fetchData = () => {
    setLoading(true);
    axios
      .get("http://localhost:3001/")
      .then(function (response) {
        console.log(response);
        setUsers(response.data);
      })
      .catch(function (error) {
        console.log(error);
      })
      .then(function () {
        setLoading(false);
      });
  };
  useEffect(() => {
    fetchData();
  }, []);

  return (
    <>
      <Layout>
        <TableContainer component={Paper} className={classes.tableContainer}>
          <Table className={classes.table} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell>username</TableCell>
                <TableCell align="left">age</TableCell>
                <TableCell align="left">phone</TableCell>
                <TableCell align="left">email</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {users.map((row) => (
                <TableRow key={row.name}>
                  <TableCell component="th" scope="row">
                    {row.username}
                  </TableCell>
                  <TableCell align="left">{row.age}</TableCell>
                  <TableCell align="left">{row.phone}</TableCell>
                  <TableCell align="left">{row.email}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
        <RefetchButton onClick={() => fetchData()}>Refetch</RefetchButton>
        <Form refetch={fetchData} />
      </Layout>
      <Backdrop open={loading} className={classes.backdrop}>
        <CircularProgress color="inherit" />
      </Backdrop>
    </>
  );
}

export default App;
