import React, { useState } from "react";
import styled from "styled-components";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import { Formik } from "formik";
import * as Yup from "yup";
import { makeStyles } from "@material-ui/core/styles";
import FormHelperText from "@material-ui/core/FormHelperText";
import axios from "axios";

const useStyles = makeStyles((theme) => ({
  textField: {
    width: "100%",
  },
  input: {
    borderRadius: "1rem",
    fontSize: "1.4rem",
    fontFamily: "Roboto Slab",
  },
  disabled: {
    backgroundColor: "#f5f5f5",
    borderRadius: "1rem",
  },
  label: {
    fontSize: "1.4rem",
    fontFamily: "Roboto Slab",
  },
  labelFocused: {
    fontSize: "1.4rem",
  },
  tooltip: {
    backgroundColor: "var(--friendly-red)",
    color: "#fff",
    fontSize: "1.4rem",
  },
}));

const Input = (props) => {
  const classes = useStyles();
  return (
    <FormControl
      variant="outlined"
      className={classes.textField}
      error={props.error}
    >
      <InputLabel
        htmlFor="component-outlined"
        classes={{ root: classes.label, focused: classes.label }}
      >
        {props.label}
      </InputLabel>
      <OutlinedInput
        multiline={props.multiline}
        rows={(props.multiline && props.rows) || 3}
        id="component-outlined"
        placeholder={props.placeholder}
        label={props.label}
        classes={{ root: classes.input, disabled: classes.disabled }}
        onChange={props.onChange}
        onBlur={props.onBlur}
        type={props.type}
        value={props.value}
        disabled={props.disabled}
        name={props.name}
        error={props.error}
      />
      {props.error && <FormHelperText>{props.error}</FormHelperText>}
    </FormControl>
  );
};

const Container = styled.div`
  width: 20%;
  padding: 2rem 2rem;
  align-items: stretch;
  border-radius: 1.2rem;
  border: solid 0.1rem rgba(13, 16, 55, 0.1);
  background-color: #ffffff;
  margin: 0 15vw;
  margin-bottom: 10rem;
`;
const ButtonBox = styled.div`
  display: flex;
  flex-direction: row-reverse;
  width: 100%;
  justify-content: space-evenly;
`;
const SubmitButton = styled.button`
  display: flex;
  justify-content: center;
  background-color: #614385;
  width: 60%;
  color: #fff;
  border-radius: 1rem;
  padding: 1.5rem 0;
  transition: background-color 0.3s ease;
  font-size: 1rem;
  cursor: pointer;
  :hover {
    opacity: 0.95;
  }
  &:disabled:hover {
    opacity: 1;
  }
`;
const CancelButton = styled(SubmitButton)`
  background-color: #516395;
  width: 30%;
`;
const FormSchema = Yup.object().shape({
  username: Yup.string().required("Required"),
  age: Yup.number().required("Required"),
  phone: Yup.string().length(11).required("Required"),
  email: Yup.string().email().required("Required"),
});

export default ({ refetch }) => {
  return (
    <Formik
      initialValues={{ username: "", age: "", phone: "", email: "" }}
      validationSchema={FormSchema}
      validateOnChange={false}
      validateOnBlur={false}
      onSubmit={({ username, age, phone, email }, { setSubmitting }) => {
        axios
          .post("http://localhost:3001/register", {
            username,
            age,
            phone,
            email,
          })
          .then(function (response) {
            console.log(response);
            refetch();
          })
          .catch(function (error) {
            console.log(error);
          });
      }}
    >
      {({
        values,
        errors,
        handleChange,
        handleBlur,
        handleSubmit,
        resetForm,
      }) => (
        <Container>
          <div style={{ marginBottom: "1.5rem" }}>
            <Input
              label="Username"
              name="username"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.username}
              error={errors.username}
            />
          </div>
          <div style={{ marginBottom: "2.5rem" }}>
            <Input
              label="Age"
              type={"number"}
              name="age"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.age}
              error={errors.age}
            />
          </div>
          <div style={{ marginBottom: "1.5rem" }}>
            <Input
              label="Phone"
              name="phone"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.phone}
              error={errors.phone}
            />
          </div>
          <div style={{ marginBottom: "2.5rem" }}>
            <Input
              label="Email"
              name="email"
              placeholder="email@example.com"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.email}
              error={errors.email}
            />
          </div>
          <ButtonBox>
            <SubmitButton onClick={handleSubmit}>Submit</SubmitButton>
            <CancelButton>Cancel</CancelButton>
          </ButtonBox>
        </Container>
      )}
    </Formik>
  );
};
